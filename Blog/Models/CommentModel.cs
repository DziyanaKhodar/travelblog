﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models
{
    public class BasicCommentModel
    {
        public string Text { get; set; }
    }

    public class DisplayCommentModel : BasicCommentModel
    {    
        public long CommentId { get; set; }
        public BasicUserModel Author { get; set; }
        public DateTimeOffset? DatePublished { get; set; }

        public static explicit operator DisplayCommentModel(Comment comment)
        {
            return new DisplayCommentModel
            {
                CommentId = comment.Id,
                DatePublished = comment.DatePublished,
                Text = comment.Text,
                Author = (BasicUserModel)comment.Author
            };
        }

        public static explicit operator Comment(DisplayCommentModel displayCommentModel)
        {
            return new Comment
            {
                Id = displayCommentModel.CommentId,
                DatePublished = displayCommentModel.DatePublished,
                Text = displayCommentModel.Text,
                AuthorId = displayCommentModel.Author.UserId
            };
        }
    }
}
