﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models
{
    public class OpenedBlogModel : BlogModel
    {
        public IEnumerable<BasicPostModel> Posts { get; set; }

        public static explicit operator OpenedBlogModel(Blogg blog)
        {
            var openedBlogModel = (OpenedBlogModel)(BlogModel)blog;
            openedBlogModel.Posts = blog.Posts?.Select(x => (BasicPostModel)x);
            return openedBlogModel;
        }
    }

    public class BlogModel
    {
        public long BlogId { get; set; }
        public string Title { get; set; }
        public BasicUserModel Owner { get; set; }

        public static explicit operator BlogModel(Blogg blog)
        {
            return new BlogModel
            {
                BlogId = blog.Id,
                Title = blog.BlogTitle,
                Owner = (BasicUserModel)blog.Owner
            };
        }
    }
}
