﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models
{
    public class PostCollection
    {
        public PostCollection(IEnumerable<Post> posts)
        {
            Posts = posts?.Select(x => (BasicPostModel)x).ToArray();
        }
        public IEnumerable<BasicPostModel> Posts { get; set; }
    }

    public class BasicPostModel
    {
        public long PostId { get; set; }
        public long BlogId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string DatePublished { get; set; }
        public BasicUserModel User { get; set; }
        public string Tags { get; set; }

        public static explicit operator BasicPostModel(Post post)
        {
            return new BasicPostModel
            {
                BlogId = post.BlogId,
                PostId = post.Id,
                Title = post.PostTitle,
                Text = string.Concat(post.PostText.Take(200), "..."),
                DatePublished = post.DatePublished.ToString(),
                User = (BasicUserModel)post.Blog.Owner,
                Tags = string.Join(" #", post.TagPosts?.Select(x => x.Tag.TagText))
            };
        }
    }

    public class PostModel : BasicPostModel
    {

        public IEnumerable<DisplayCommentModel> Comments { get; set; }

        public static explicit operator PostModel(Post post)
        {
            var postModel = (PostModel)(BasicPostModel)post;
            postModel.Comments = post.Comments?.Select(x => (DisplayCommentModel)x);
            postModel.Text = post.PostText;
            return postModel;
        }

    }
}
