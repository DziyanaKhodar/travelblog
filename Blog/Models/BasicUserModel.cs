﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models
{
    public class BasicUserModel
    {
        public long UserId { get; set; }
        public string UserName { get; set; }

        public static explicit operator BasicUserModel(User user)
        {
            return new BasicUserModel
            {
                UserId = user.Id,
                UserName = user.UserName
            };
        }
    }
}
