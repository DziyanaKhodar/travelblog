﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Blog
{
    public class Extensions
    {
        public static IEnumerable<string> ConvertStringToTagValues(string str)
        {
            if(string.IsNullOrWhiteSpace(str) || !str.Contains("#"))
            {
                return Enumerable.Empty<string>();
            }

            var withRemovedFirstWordsIfNeeded = Regex.Replace(str, "^[^#]+#", string.Empty).Trim();
            var parts = withRemovedFirstWordsIfNeeded.Split("#", StringSplitOptions.RemoveEmptyEntries);
            var processedParts = parts
                .Select(p => p.Trim())
                .Select(p => Regex.Replace(p, @"[/s/n/r/t]+", "_").ToLower());

            return processedParts;
        }
    }
}
