﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Blog.Migrations
{
    public partial class RenamedColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagPost_Posts_PostId",
                table: "TagPost");

            migrationBuilder.DropForeignKey(
                name: "FK_TagPost_Tags_TagId",
                table: "TagPost");

            migrationBuilder.RenameColumn(
                name: "PostId",
                table: "TagPost",
                newName: "TagPost_PostId");

            migrationBuilder.RenameColumn(
                name: "TagId",
                table: "TagPost",
                newName: "TagPost_TagId");

            migrationBuilder.RenameIndex(
                name: "IX_TagPost_PostId",
                table: "TagPost",
                newName: "IX_TagPost_TagPost_PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_TagPost_Posts_TagPost_PostId",
                table: "TagPost",
                column: "TagPost_PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagPost_Tags_TagPost_TagId",
                table: "TagPost",
                column: "TagPost_TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TagPost_Posts_TagPost_PostId",
                table: "TagPost");

            migrationBuilder.DropForeignKey(
                name: "FK_TagPost_Tags_TagPost_TagId",
                table: "TagPost");

            migrationBuilder.RenameColumn(
                name: "TagPost_PostId",
                table: "TagPost",
                newName: "PostId");

            migrationBuilder.RenameColumn(
                name: "TagPost_TagId",
                table: "TagPost",
                newName: "TagId");

            migrationBuilder.RenameIndex(
                name: "IX_TagPost_TagPost_PostId",
                table: "TagPost",
                newName: "IX_TagPost_PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_TagPost_Posts_PostId",
                table: "TagPost",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TagPost_Tags_TagId",
                table: "TagPost",
                column: "TagId",
                principalTable: "Tags",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
