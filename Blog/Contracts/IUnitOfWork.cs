﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Data;

namespace Blog.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
        IBlogRepository BlogRepository { get; }
        ICommentRepository CommentRepository { get; }
        IPostRepository PostRepository { get; }
        ITagRepository TagRepository { get; }
        IUserRepository UserRepository { get; }
    }
}
