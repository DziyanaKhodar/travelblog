﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;

namespace Blog.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IEnumerable<Comment> GetUserComments(long userId, int? page = null);
        IEnumerable<Comment> GetPostComments(long postId);
    }
}
