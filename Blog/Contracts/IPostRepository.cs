﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;

namespace Blog.Contracts
{
    public interface IPostRepository : IRepository<Post>
    {
        IEnumerable<Post> GetPostsByTags(string[] tags, int? page = null);
        IEnumerable<Post> GetPostsContainingWords(string[] words, int? page = null);
        IEnumerable<Post> GetPostsByAuthor(string userName, int? page = null);
        IEnumerable<Post> GetPostsByAuthor(long userId, int? page = null);
        IEnumerable<Post> GetPostsByTagsOrWords(string[] tags, string[] words, int? page);
    }
}
