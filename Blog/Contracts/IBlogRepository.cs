﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;

namespace Blog.Contracts
{
    public interface IBlogRepository : IRepository<Blogg>
    {
        IEnumerable<Blogg> GetBlogsByTitle(string title, int? page = null);
        IEnumerable<Blogg> GetBlogsByOwner(long userId);
        IEnumerable<Blogg> GetBlogByOwnerName(string userName, int? page = null);
    }
}
