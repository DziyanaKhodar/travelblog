﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;

namespace Blog.Contracts
{
    public interface IUserRepository : IRepository<User> 
    {
    }
}
