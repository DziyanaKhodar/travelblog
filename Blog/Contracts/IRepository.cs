﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Data;

namespace Blog.Contracts
{
    public interface IRepository<T> where T:Entity
    {
        T GetById(long id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void CreateOrUpdate(T entity);
    }
}
