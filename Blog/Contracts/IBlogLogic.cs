﻿using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Contracts
{
    public interface IBlogLogic : IDisposable
    {
        void AddTagsToPost(Post post, IEnumerable<string> tags);
    }
}
