﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Contracts;
using Blog.Data.Models;

namespace Blog
{
    public class BlogLogic : IBlogLogic, IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;

        public BlogLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddCommentToPost(long postId, string commentText, long authorId)
        {
            var comment = new Comment
            {
                Text = commentText,
                AuthorId = authorId,
                DatePublished = DateTimeOffset.Now.UtcDateTime,
                PostId = postId
            };

            _unitOfWork.CommentRepository.Add(comment);
        }

        public void AddTagsToPost(Post post, IEnumerable<string> tags)
        {
            if (post == null)
                return;

            var distinctTags = tags.Distinct();

            foreach(var tag in distinctTags)
            {
                var tagToAdd = _unitOfWork.TagRepository.GetTagByText(tag);
                if(tagToAdd == null)
                {
                    tagToAdd = new Tag
                    {
                        TagText = tag
                    };
                }
                post.TagPosts.Add(
                    new TagPost
                    {
                        Post = post,
                        Tag = tagToAdd
                    });
            }

        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
