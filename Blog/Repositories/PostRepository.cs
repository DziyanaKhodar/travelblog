﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Contracts;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace Blog.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        public PostRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }

        public override Post GetById(long id)
        {
            return _context.Posts
                .Where(x => x.Id == id)
                .Include(x => x.TagPosts)
                .Include(x => x.Comments)
                .Include(x => x.Blog)
                .FirstOrDefault();
        }

        public IEnumerable<Post> GetPostsByAuthor(long userId, int? page = null)
        {
            var result = _context.Posts.Where(p => p.Blog.OwnerId == userId);
            return GetPage(result, page);
        }

        public IEnumerable<Post> GetPostsByAuthor(string userName, int? page = null)
        {
            var result = _context.Posts.Where(p => p.Blog.Owner.UserName.Contains(userName));
            return GetPage(result, page);
        }

        public IEnumerable<Post> GetPostsByTags(string[] tags, int? page = null)
        {
            return GetPage(GetPostsByTags(tags), page);
        }

        private IQueryable<Post> GetPostsByTags(string[] tags)
        {
            var result = _context.Posts
                .FromSql(GetQueryForTagsTextSearch(tags));
            return result;
        }

        private IQueryable<Post> GetPostsContainingWords(string[] words)
        {
            var result = _context.Posts
                .FromSql(GetQueryForPostsTextSearch(words));
            return result;
        }

        public IEnumerable<Post> GetPostsByTagsOrWords(string [] tags, string[] words, int? page)
        {
            var postsByTags = GetPostsByTags(tags);
            var postsByWords = GetPostsContainingWords(words);
            var result = postsByTags.Union(postsByWords);
            return GetPage(result, page);
        }

        public IEnumerable<Post> GetPostsContainingWords(string[] words, int? page = null)
        {
            return GetPage(GetPostsContainingWords(words), page);
        }

        private string GetTextSearchQuery(string baseQuery, string likeQuery, string[] words)
        {
            words = words.Select(w => "'%" + w.Trim() + "%'").Distinct(StringComparer.Ordinal).ToArray();
            var likeQueries = words.Select(w => string.Format(likeQuery, w, w));

            return string.Concat
                (
                    baseQuery,
                    string.Join(" OR ", likeQueries)
                );
        }

        private string GetQueryForTagsTextSearch(string[] words)
        {
            string baseQuery = @"SELECT [dbo].[Posts].*
                    FROM [dbo].[TagPost] 
                    JOIN [dbo].[Tags] ON [dbo].[TagPost].TagPost_TagId = [dbo].[Tags].Id
                    JOIN [dbo].[Posts] ON [dbo].[TagPost].TagPost_PostId = [dbo].[Posts].Id
                    WHERE ";
            var likeQuery = "TagText LIKE {0}";
            return GetTextSearchQuery(baseQuery, likeQuery, words);
        }

        private string GetQueryForPostsTextSearch(string[] words)
        {
            string baseQuery = @"SELECT *                     
                    FROM [dbo].[Posts]
                    WHERE ";
            var likeQuery = "PostText LIKE {0} OR PostTitle LIKE {1}";
            return GetTextSearchQuery(baseQuery, likeQuery, words);
           
        }
    }
}
