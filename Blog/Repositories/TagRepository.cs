﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Contracts;
using Blog.Data;

namespace Blog.Repositories
{
    public class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(BlogDbContext blogDbContext):base(blogDbContext)
        {

        }
        public IEnumerable<Tag> GetPostTags(long postId)
        {
            return _context.Tags.Where(t => t.TagPosts.Any(tp => tp.PostId == postId));
        }

        public Tag GetTagByText(string text)
        {
            return _context.Tags.Where(t => t.TagText == text).FirstOrDefault();
        }
    }
}
