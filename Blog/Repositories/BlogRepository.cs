﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Contracts;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace Blog.Repositories
{
    public class BlogRepository : Repository<Blogg>, IBlogRepository
    {
        public BlogRepository(BlogDbContext dbContext):base(dbContext)
        {

        }

        public override Blogg GetById(long id)
        {
            return _context.Blogs
                        .Where(x => x.Id == id)
                        .Include(x => x.Owner)
                        .FirstOrDefault();
        }

        public IEnumerable<Blogg> GetBlogByOwnerName(string userName, int? page = null)
        {
            var result = _context.Blogs.Where(blog => blog.Owner.UserName.Contains(userName));
            return GetPage(result, page);
        }

        public IEnumerable<Blogg> GetBlogsByOwner(long userId)
        {
            var allBlogs =_context.Blogs.Where(b => b.OwnerId == userId).ToArray();
            return allBlogs;
        }

        public IEnumerable<Blogg> GetBlogsByTitle(string titlePart, int? page = null)
        {
            var allBlogs = _context.Blogs.Where(b => b.BlogTitle.Contains(titlePart));
            return GetPage(allBlogs);
        }
    }
}
