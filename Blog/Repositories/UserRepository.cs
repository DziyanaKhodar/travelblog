﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Contracts;
using Blog.Data;

namespace Blog.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }
    }
}
