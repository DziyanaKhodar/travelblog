﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Data.Models;
using Blog.Contracts;
using Blog.Data;
using Microsoft.EntityFrameworkCore;

namespace Blog.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(BlogDbContext blogDbContext) : base(blogDbContext)
        {

        }

        public override Comment GetById(long id)
        {
            return _context.Comments
                .Where(x => x.Id == id)
                .Include(x => x.Author)
                .FirstOrDefault();

        }

        public IEnumerable<Comment> GetPostComments(long postId)
        {
            return _context.Comments.Where(x => x.PostId == postId).ToArray();
        }

        public IEnumerable<Comment> GetUserComments(long userId, int? page = null)
        {
            var res = _context.Comments.Where(x => x.AuthorId == userId);
            return GetPage(res, page);

        }
    }
}
