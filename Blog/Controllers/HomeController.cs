﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Blog.Models;
using Blog.Contracts;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
           // var r =_unitOfWork.BlogRepository.GetBlogsByTitle("title");
            var z = _unitOfWork.PostRepository.GetPostsContainingWords(new[] { "a", "bc", "B" }).ToList();
            var k = _unitOfWork.PostRepository.GetPostsByTags(new[] { "a" }).ToList();

            foreach (var e in z) ;
            //var z = _unitOfWork._context.Users.Where(u => u.UserName == "uname");
            //foreach (var x in r) ;
            //foreach (var x in z) ;
            //_unitOfWork._context.Dispose();
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
