﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blog.Contracts;
using Blog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Blog.Controllers
{
    public class PostController : Controller
    {

        private readonly IBlogLogic _blogLogic;
        private readonly IUnitOfWork _unitOfWork;
        private readonly long _currentUserId;
        public PostController(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _blogLogic = new BlogLogic(unitOfWork);
            _unitOfWork = unitOfWork;
            _currentUserId = 1;//httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }


        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetCurrentUserPosts()
        {
            var posts = _unitOfWork.PostRepository.GetPostsByAuthor(_currentUserId);
            var model = new PostCollection(posts);

            return View(model);
        }


        [HttpGet]
        public IActionResult Post(long id)
        {
            var post = _unitOfWork.PostRepository.GetById(id);
            var model = (PostModel)post;
            return View(model);
        }

        //[HttpPost]
        //public IActionResult Post(PostModel postModel)
        //{

        //}


        public IActionResult AddTag(long id, string tag)
        {
            _blogLogic.AddTagsToPost(id, new[] { tag });
            return View();
        }

        public IActionResult AddPost()
        {
            _unitOfWork.PostRepository.Add(new Data.Models.Post { PostText = "ttt", Blog = new Data.Models.Blogg { Owner = new Data.Models.User { UserName = "username" } } });
            _unitOfWork.Save();
            return View();

        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            _blogLogic.Dispose();
            base.Dispose(disposing);
        }
    }
}
