﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Blog
{
    internal class DbFileLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly string _logFilePath;

        public DbFileLogger(string categoryName, string logFilePath)
        {
            _categoryName = categoryName;
            _logFilePath = logFilePath;
        }

        public IDisposable BeginScope<TState>(TState state) => null;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (string.IsNullOrEmpty(_logFilePath) == false)
            {
                var mes = $"{ logLevel } { eventId.Id} { _categoryName}" + formatter(state, exception);
                File.AppendAllText(_logFilePath, mes);
            }
        }
    }

    internal class DbFileLoggerProvider : ILoggerProvider
    {
        private const string LogFileName = "DatabaseLogs.txt";
        private readonly string _logFilePath;
        private readonly ILogger _emergencyLog;

        public DbFileLoggerProvider(string logFilePath)//, ILogger emergencyLog)
        {
            //_emergencyLog = emergencyLog;
            _logFilePath = GetLogfile(logFilePath);
        }

        private string GetLogfile(string logPath)
        {
            if (string.IsNullOrEmpty(logPath))
            {
                //_emergencyLog.NonCriticalErrorEvent(
                //    "Database log file path is not provided",
                //    "Database logs won't be written",
                //    "Check the database log path in TaskSettings.json file."
                //);
                return string.Empty;
            }

            try
            {
                if (Directory.Exists(logPath))
                    return Path.Combine(logPath, LogFileName);

                Directory.CreateDirectory(logPath);
                return Path.Combine(logPath, LogFileName);
            }
            catch (Exception ex)
            {
                throw;
                //_emergencyLog.CriticalErrorEvent(
                //    $"Couldn't create database log file {logPath.GetFullPath(_emergencyLog)}.",
                //    "Check the database log path in TaskSettings.json file."
                //);
                //throw new StopProcessingException("Database log file initialize error", ex);
            }
        }

        public ILogger CreateLogger(string categoryName) => new DbFileLogger(categoryName, _logFilePath);

        public void Dispose()
        {
        }
    }
}
