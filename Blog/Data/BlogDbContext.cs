﻿using System;
using System.Collections.Generic;
using System.Text;
using Blog.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Blog.Data.EntitiesConfiguration;
using Microsoft.Extensions.Logging;

namespace Blog.Data
{
    public class BlogDbContext : DbContext//: IdentityDbContext
    {
        public BlogDbContext(DbContextOptions<BlogDbContext> options) : base(options)
        {

        }

        //public BlogDbContext():base()
        //{

        //}

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Blogg> Blogs { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connectionString = @"Data Source=MY-PC\SQLEXPRESS;Initial Catalog=TestBlog;Integrated Security=SSPI;";//_settings?.ConnectionString;

                //if (string.IsNullOrEmpty(connectionString))
                //{
                //    _logger.CriticalErrorEvent(
                //        "Sqlite database file connection string is not provided.",
                //        "Check the connection string in TaskSettings.json file."
                //    );
                //    throw new StopProcessingException("Database connection string not found");
                //}

                optionsBuilder.UseSqlServer(connectionString);
            }

            //if (string.IsNullOrWhiteSpace(_settings.DatabaseLogPath) == false)
            //{
                var loggerFactory = new LoggerFactory();
                loggerFactory.AddProvider(new DbFileLoggerProvider("logs"));
                optionsBuilder.UseLoggerFactory(loggerFactory);
            //}

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);  
            builder.ApplyConfiguration(new UserEntityConfiguration());
            builder.ApplyConfiguration(new BlogEntityConfiguration());
            builder.ApplyConfiguration(new PostEntityConfiguration());
            builder.ApplyConfiguration(new CommentEntityConfiguration());
            builder.ApplyConfiguration(new TagEntityConfiguration());
            builder.ApplyConfiguration(new TagPostEntityConfiguration());
        }
    }
}
