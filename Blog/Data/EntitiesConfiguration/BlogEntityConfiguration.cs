﻿using Microsoft.EntityFrameworkCore;
using Blog.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blog.Data.EntitiesConfiguration
{
    public class BlogEntityConfiguration : IEntityTypeConfiguration<Blog.Data.Models.Blogg>
    {
        public void Configure(EntityTypeBuilder<Blog.Data.Models.Blogg> builder)
        {
            builder.HasKey(b => b.Id).HasName("BlogId");
            builder.HasOne(b => b.Owner)
                .WithMany(o => o.Blogs);
            builder.HasMany(b => b.Posts)
                .WithOne(p => p.Blog);
        }
    }
}
