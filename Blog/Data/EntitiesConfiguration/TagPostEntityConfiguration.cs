﻿using Blog.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.EntitiesConfiguration
{
    public class TagPostEntityConfiguration : IEntityTypeConfiguration<TagPost>
    {
        
        public void Configure(EntityTypeBuilder<TagPost> builder)
        {
            builder.HasKey(tp => new
            {
                tp.TagId,
                tp.PostId
            });
        }
    }
}
