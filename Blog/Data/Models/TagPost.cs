﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.Data.Models
{
    public class TagPost
    {
        [Column("TagPost_TagId")]
        public long TagId { get; set; }
        [Column("TagPost_PostId")]
        public long PostId { get; set; }
        public virtual Tag Tag { get; set; }
        public virtual Post Post { get; set; }
    }
}
