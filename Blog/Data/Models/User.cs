﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Blog.Data.Models
{
    public class User : Entity
    {
        public User():base()
        {
            Blogs = new HashSet<Blogg>();
            Comments = new HashSet<Comment>();
        }
        public virtual ICollection<Blogg> Blogs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public string UserName { get;  set; }
    }
}
