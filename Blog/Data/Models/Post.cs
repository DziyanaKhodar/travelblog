﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class Post : Entity
    {
        public Post()
        {
            TagPosts = new HashSet<TagPost>();
            Comments = new HashSet<Comment>();
        }
        public string PostTitle { get; set; }
        public string PostText { get; set; }
        public DateTimeOffset? DatePublished { get; set; }

        public long BlogId { get; set; }
        public virtual Blogg Blog { get; set; }
        public virtual ICollection<TagPost> TagPosts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
