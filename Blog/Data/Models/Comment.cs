﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class Comment : Entity
    {
        public string Text { get; set; }
        public DateTimeOffset? DatePublished { get; set; }

        public long? AuthorId { get; set; }
        public long PostId { get; set; }
        public virtual User Author { get; set; }
        public virtual Post Post { get; set; }
    }
}
