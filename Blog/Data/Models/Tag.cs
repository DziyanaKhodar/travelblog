﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class Tag : Entity
    {
        public Tag()
        {

            TagPosts = new HashSet<TagPost>();
        }

        public string TagText { get; set; }
        public virtual ICollection<TagPost> TagPosts { get; set; }
        //public virtual ICollection<Post> Posts { get; set; }
    }

}
