﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Data.Models
{
    public class Blogg : Entity
    { 
        public Blogg()
        {
            Posts = new HashSet<Post>();
        }

        public string BlogTitle { get; set; }

        public long OwnerId { get; set; }
        public virtual User Owner { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}
